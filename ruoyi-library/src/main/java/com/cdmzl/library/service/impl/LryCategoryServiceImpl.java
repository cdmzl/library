package com.cdmzl.library.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cdmzl.common.utils.TreeBuildUtils;
import com.cdmzl.library.domain.LryCategory;
import com.cdmzl.library.mapper.LryCategoryMapper;
import com.cdmzl.library.service.LryCategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (LryCategory)表服务实现类
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:43
 */
@Slf4j
@Service("lryCategoryService")
public class LryCategoryServiceImpl extends ServiceImpl<LryCategoryMapper, LryCategory> implements LryCategoryService {


    @Override
    public List list(LryCategory bean) {
        return baseMapper.selectList(bean);
    }

    @Override
    public List selectCategoryTreeList(LryCategory bean) {
        return this.buildCategoryTreeSelect(baseMapper.selectList(bean));
    }

    public List<Tree<Long>> buildCategoryTreeSelect(List<LryCategory> categoryList) {
        if (CollUtil.isEmpty(categoryList)) {
            return CollUtil.newArrayList();
        }
        return TreeBuildUtils.build(categoryList, (bean, tree) ->
                tree.setId(bean.getCategoryId())
                        .setParentId(bean.getParentId())
                        .setName(bean.getCategoryName())
                        .setWeight(bean.getOrderNum()));
    }

}

