package com.cdmzl.library.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cdmzl.library.domain.LrySeat;

/**
 * (LrySeat)表服务接口
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:43
 */
public interface LrySeatService extends IService<LrySeat> {

}

