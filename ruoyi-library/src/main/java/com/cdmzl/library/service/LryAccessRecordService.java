package com.cdmzl.library.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cdmzl.library.domain.LryAccessRecord;

public interface LryAccessRecordService extends IService<LryAccessRecord> {
}
