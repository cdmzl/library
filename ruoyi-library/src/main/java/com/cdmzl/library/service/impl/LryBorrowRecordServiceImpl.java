package com.cdmzl.library.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cdmzl.library.domain.LryBorrowRecord;
import com.cdmzl.library.mapper.LryBorrowRecordMapper;
import com.cdmzl.library.service.LryBorrowRecordService;
import org.springframework.stereotype.Service;

/**
 * (LryBorrowRecord)表服务实现类
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:42
 */
@Service("lryBorrowRecordService")
public class LryBorrowRecordServiceImpl extends ServiceImpl<LryBorrowRecordMapper, LryBorrowRecord> implements LryBorrowRecordService {


}

