package com.cdmzl.library.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cdmzl.library.domain.LryBorrowRecord;

/**
 * (LryBorrowRecord)表服务接口
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:42
 */
public interface LryBorrowRecordService extends IService<LryBorrowRecord> {



}

