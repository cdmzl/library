package com.cdmzl.library.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cdmzl.library.domain.LrySeat;
import com.cdmzl.library.mapper.LrySeatMapper;
import com.cdmzl.library.service.LrySeatService;
import org.springframework.stereotype.Service;

/**
 * (LrySeat)表服务实现类
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:43
 */
@Service("lrySeatService")
public class LrySeatServiceImpl extends ServiceImpl<LrySeatMapper, LrySeat> implements LrySeatService {

}

