package com.cdmzl.library.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cdmzl.library.domain.LryBook;
import com.cdmzl.library.mapper.LryBookMapper;
import com.cdmzl.library.service.LryBookService;
import org.springframework.stereotype.Service;

/**
 * (LryBook)表服务实现类
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:41
 */
@Service("lryBookService")
public class LryBookServiceImpl extends ServiceImpl<LryBookMapper, LryBook> implements LryBookService {

}

