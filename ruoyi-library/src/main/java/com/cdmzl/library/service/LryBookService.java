package com.cdmzl.library.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cdmzl.library.domain.LryBook;

/**
 * (LryBook)表服务接口
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:41
 */
public interface LryBookService extends IService<LryBook> {

}

