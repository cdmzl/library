package com.cdmzl.library.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cdmzl.library.domain.LryAccessRecord;
import com.cdmzl.library.mapper.LryAccessRecordMapper;
import com.cdmzl.library.service.LryAccessRecordService;
import org.springframework.stereotype.Service;

@Service
public class LryAccessRecordServiceImpl extends ServiceImpl<LryAccessRecordMapper, LryAccessRecord> implements LryAccessRecordService {
}
