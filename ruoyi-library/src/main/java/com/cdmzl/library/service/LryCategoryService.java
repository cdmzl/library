package com.cdmzl.library.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cdmzl.library.domain.LryCategory;

import java.util.List;

/**
 * (LryCategory)表服务接口
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:43
 */
public interface LryCategoryService extends IService<LryCategory> {

    List<Tree<Long>> selectCategoryTreeList(LryCategory bean);

    List list(LryCategory bean);
}

