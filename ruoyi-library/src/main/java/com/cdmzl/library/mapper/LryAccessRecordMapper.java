package com.cdmzl.library.mapper;

import com.cdmzl.common.core.mapper.BaseMapperPlus;
import com.cdmzl.library.domain.LryAccessRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * (LryAccessRecord)表数据库访问层
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:42
 */
@Mapper
public interface LryAccessRecordMapper extends BaseMapperPlus<LryAccessRecordMapper, LryAccessRecord> {

}

