package com.cdmzl.library.mapper;

import com.cdmzl.common.core.mapper.BaseMapperPlus;
import com.cdmzl.library.domain.LryCategory;
import org.apache.ibatis.annotations.Mapper;

/**
 * (LryCategory)表数据库访问层
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:43
 */
@Mapper
public interface LryCategoryMapper extends BaseMapperPlus<LryCategoryMapper, LryCategory> {

}

