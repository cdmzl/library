package com.cdmzl.library.mapper;

import com.cdmzl.common.core.mapper.BaseMapperPlus;
import com.cdmzl.library.domain.LrySeat;
import org.apache.ibatis.annotations.Mapper;

/**
 * (LrySeat)表数据库访问层
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:43
 */
@Mapper
public interface LrySeatMapper extends BaseMapperPlus<LrySeatMapper, LrySeat> {

}

