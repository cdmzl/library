package com.cdmzl.library.mapper;

import com.cdmzl.common.core.mapper.BaseMapperPlus;
import com.cdmzl.library.domain.LryBook;
import org.apache.ibatis.annotations.Mapper;

/**
 * (LryBook)表数据库访问层
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:42
 */
@Mapper
public interface LryBookMapper extends BaseMapperPlus<LryBookMapper, LryBook> {

}

