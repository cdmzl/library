package com.cdmzl.library.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cdmzl.common.core.controller.BaseController;
import com.cdmzl.common.core.domain.R;
import com.cdmzl.library.domain.LryBorrowRecord;
import com.cdmzl.library.service.LryBorrowRecordService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Arrays;

/**
 * (LryBorrowRecord)表控制层
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:42
 */
@Api(value = "", tags = {""})
@RequiredArgsConstructor
@RestController
@RequestMapping("library/borrow")
public class LryBorrowRecordController extends BaseController {

    private final LryBorrowRecordService lryBorrowRecordService;

    /**
     * 分页查询所有数据
     *
     * @param page            分页对象
     * @param lryBorrowRecord 查询实体
     * @return 所有数据
     */
    @GetMapping
    public R selectAll(Page<LryBorrowRecord> page, LryBorrowRecord lryBorrowRecord) {
        return R.ok(this.lryBorrowRecordService.page(page, new QueryWrapper<>(lryBorrowRecord)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("getById/{id}")
    public R queryById(@PathVariable Serializable id) {
        return R.ok(this.lryBorrowRecordService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param lryBorrowRecord 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R add(@RequestBody LryBorrowRecord lryBorrowRecord) {
        return R.ok(this.lryBorrowRecordService.save(lryBorrowRecord));
    }

    /**
     * 修改数据
     *
     * @param lryBorrowRecord 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R edit(@RequestBody LryBorrowRecord lryBorrowRecord) {
        return R.ok(this.lryBorrowRecordService.updateById(lryBorrowRecord));
    }

    /**
     * 删除数据
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R deleteByIds(@RequestBody Long[] ids) {
        return R.ok(this.lryBorrowRecordService.removeByIds(Arrays.asList(ids)));
    }
}

