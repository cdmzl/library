package com.cdmzl.library.controller;

import com.cdmzl.common.core.controller.BaseController;
import com.cdmzl.common.core.domain.R;
import com.cdmzl.library.domain.LryCategory;
import com.cdmzl.library.service.LryCategoryService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Arrays;

/**
 * (LryCategory)表控制层
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:43
 */
@Api(value = "", tags = {""})
@RequiredArgsConstructor
@RestController
@RequestMapping("library/category")
public class LryCategoryController extends BaseController {

    private final LryCategoryService lryCategoryService;

    /**
     * 分页查询所有数据
     *
     * @param lryCategory 查询实体
     * @return 所有数据
     */
    @GetMapping("/list")
    public R selectAll(LryCategory lryCategory) {
        return R.ok(this.lryCategoryService.list(lryCategory));
    }

    /**
     * 分页查询所有数据
     *
     * @param lryCategory 查询实体
     * @return 所有数据
     */
    @GetMapping("/selectCategoryTreeList")
    public R selectCategoryTreeList(LryCategory lryCategory) {
        return R.ok(this.lryCategoryService.selectCategoryTreeList(lryCategory));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("getById/{id}")
    public R queryById(@PathVariable Serializable id) {
        return R.ok(this.lryCategoryService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param lryCategory 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R add(@RequestBody LryCategory lryCategory) {
        this.lryCategoryService.save(lryCategory);
        return R.ok();
    }

    /**
     * 修改数据
     *
     * @param lryCategory 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R edit(@RequestBody LryCategory lryCategory) {
        return R.ok(this.lryCategoryService.updateById(lryCategory));
    }

    /**
     * 删除数据
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R deleteByIds(@RequestBody Long[] ids) {
        return R.ok(this.lryCategoryService.removeByIds(Arrays.asList(ids)));
    }
}

