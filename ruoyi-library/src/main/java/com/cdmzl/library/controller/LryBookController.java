package com.cdmzl.library.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cdmzl.common.core.controller.BaseController;
import com.cdmzl.common.core.domain.R;
import com.cdmzl.library.domain.LryBook;
import com.cdmzl.library.service.LryBookService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Arrays;

/**
 * (LryBook)表控制层
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:41
 */
@Api(value = "", tags = {""})
@RequiredArgsConstructor
@RestController
@RequestMapping("library/book")
public class LryBookController extends BaseController {

    private final LryBookService lryBookService;

    /**
     * 分页查询所有数据
     *
     * @param page    分页对象
     * @param lryBook 查询实体
     * @return 所有数据
     */
    @GetMapping("/page")
    public R selectAll(Page<LryBook> page, LryBook lryBook) {
        return R.ok(this.lryBookService.page(page, new QueryWrapper<>(lryBook)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("getById/{id}")
    public R queryById(@PathVariable Serializable id) {
        return R.ok(this.lryBookService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param lryBook 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R add(@RequestBody LryBook lryBook) {
        return R.ok(this.lryBookService.save(lryBook));
    }

    /**
     * 修改数据
     *
     * @param lryBook 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R edit(@RequestBody LryBook lryBook) {
        return R.ok(this.lryBookService.updateById(lryBook));
    }

    /**
     * 删除数据
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R deleteByIds(@RequestBody Long[] ids) {
        return R.ok(this.lryBookService.removeByIds(Arrays.asList(ids)));
    }
}

