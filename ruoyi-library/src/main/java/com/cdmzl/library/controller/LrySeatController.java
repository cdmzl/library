package com.cdmzl.library.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cdmzl.common.core.controller.BaseController;
import com.cdmzl.common.core.domain.R;
import com.cdmzl.library.domain.LrySeat;
import com.cdmzl.library.service.LrySeatService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Arrays;

/**
 * (LrySeat)表控制层
 *
 * @author XiaoJiang
 * @since 2024-05-22 16:18:43
 */
@Api(value = "", tags = {""})
@RequiredArgsConstructor
@RestController
@RequestMapping("library/seat")
public class LrySeatController extends BaseController {

    private final LrySeatService lrySeatService;

    /**
     * 分页查询所有数据
     *
     * @param page    分页对象
     * @param lrySeat 查询实体
     * @return 所有数据
     */
    @GetMapping
    public R selectAll(Page<LrySeat> page, LrySeat lrySeat) {
        return R.ok(this.lrySeatService.page(page, new QueryWrapper<>(lrySeat)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("getById/{id}")
    public R queryById(@PathVariable Serializable id) {
        return R.ok(this.lrySeatService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param lrySeat 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R add(@RequestBody LrySeat lrySeat) {
        return R.ok(this.lrySeatService.save(lrySeat));
    }

    /**
     * 修改数据
     *
     * @param lrySeat 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R edit(@RequestBody LrySeat lrySeat) {
        return R.ok(this.lrySeatService.updateById(lrySeat));
    }

    /**
     * 删除数据
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R deleteByIds(@RequestBody Long[] ids) {
        return R.ok(this.lrySeatService.removeByIds(Arrays.asList(ids)));
    }
}

