package com.cdmzl.library.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cdmzl.common.core.domain.R;
import com.cdmzl.library.domain.LryAccessRecord;
import com.cdmzl.library.service.LryAccessRecordService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Arrays;

@RequiredArgsConstructor
@RestController
@RequestMapping("library/access")
public class LryAccessRecordController {

    private final LryAccessRecordService accessRecordService;

    /**
     * 分页查询所有数据
     *
     * @param bean 查询实体
     * @return 所有数据
     */
    @GetMapping("/page")
    public R selectAll(Page page, LryAccessRecord bean) {
        return R.ok(this.accessRecordService.page(page, Wrappers.query(bean)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("getById/{id}")
    public R queryById(@PathVariable Serializable id) {
        return R.ok(this.accessRecordService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param bean 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R add(@RequestBody LryAccessRecord bean) {
        this.accessRecordService.save(bean);
        return R.ok();
    }

    /**
     * 修改数据
     *
     * @param bean 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R edit(@RequestBody LryAccessRecord bean) {
        return R.ok(this.accessRecordService.updateById(bean));
    }

    /**
     * 删除数据
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R deleteByIds(@RequestBody Long[] ids) {
        return R.ok(this.accessRecordService.removeByIds(Arrays.asList(ids)));
    }
}
