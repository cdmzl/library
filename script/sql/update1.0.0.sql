-- 菜单 SQL
insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793639944523976704,'图书', '1', '1', 'book', 'book/book/index', 1, 0, 'C', '0', '0', 'book:book:list', '#', 'admin', sysdate(), '', null, '图书菜单');

-- 按钮父菜单ID
SELECT @parentId := 1793639944523976704;

-- 按钮 SQL
insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793639944523976705,'图书查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'book:book:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793639944523976706,'图书新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'book:book:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793639944523976707,'图书修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'book:book:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793639944523976708,'图书删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'book:book:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793639944523976709,'图书导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'book:book:export',       '#', 'admin', sysdate(), '', null, '');

-- 菜单 SQL
insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793643077153165312,'类别', '3', '1', 'category', 'library/category/index', 1, 0, 'C', '0', '0', 'library:category:list', '#', 'admin', sysdate(), '', null, '类别菜单');

-- 按钮父菜单ID
SELECT @parentId := 1793643077153165312;

-- 按钮 SQL
insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793643077153165313,'类别查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'library:category:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793643077153165314,'类别新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'library:category:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793643077153165315,'类别修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'library:category:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793643077153165316,'类别删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'library:category:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_id,menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(1793643077153165317,'类别导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'library:category:export',       '#', 'admin', sysdate(), '', null, '');
