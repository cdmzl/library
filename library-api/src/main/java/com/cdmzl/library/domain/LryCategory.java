package com.cdmzl.library.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.cdmzl.common.actable.annotation.Column;
import com.cdmzl.common.actable.annotation.Like;
import com.cdmzl.common.actable.annotation.Table;
import com.cdmzl.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;


/**
 * 书籍分类
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Table
public class LryCategory extends BaseEntity {


    @TableId
    private Long categoryId;


    /**
     * 类别名称
     */
    @Like
    private String categoryName;

    /**
     * 图片
     */
    private String pic;

    /**
     * 父类ID
     */
    @Column(defaultValue = "0")
    private Long parentId;

    /**
     * 分类等级
     */
    @Column(defaultValue = "0")
    private Integer level;

    /**
     * 删除标记
     */
    @TableLogic
    @Column(defaultValue = "1")
    private String tag;


    /**
     * 排序
     */
    private Integer orderNum;

}
