package com.cdmzl.library.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.cdmzl.common.actable.annotation.Column;
import com.cdmzl.common.actable.annotation.Table;
import com.cdmzl.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 图书馆座位
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Table
public class LrySeat extends BaseEntity {

    @TableId
    private Long seatId;

    /**
     * 楼层
     */
    private Integer floor;

    /**
     * 座位编号
     */
    private String seatNum;

    /**
     * 状态 空闲 有人 损坏
     */
    private String status;

    /**
     * 删除标记
     */
    @TableLogic
    @Column(defaultValue = "1")
    private String tag;
}
