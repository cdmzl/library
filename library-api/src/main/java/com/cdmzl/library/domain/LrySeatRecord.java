package com.cdmzl.library.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.cdmzl.common.actable.annotation.Column;
import com.cdmzl.common.actable.annotation.Table;
import com.cdmzl.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 座位预约记录
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Table(comment = "座位预约记录")
public class LrySeatRecord extends BaseEntity {

    @TableId
    private Long seatRecordId;

    /**
     * 入座时间
     */
    private Date startTime;

    /**
     * 离开时间
     */
    private Date endTime;

    /**
     * 记录状态 1预约 2入住 3离开 4临时离开
     */
    private String status;

    /**
     * 座位号
     */
    private Long seatId;

    /**
     * 阅读人员
     */
    private Long userId;

    /**
     * 删除标记
     */
    @TableLogic
    @Column(defaultValue = "1")
    private String tag;
}
