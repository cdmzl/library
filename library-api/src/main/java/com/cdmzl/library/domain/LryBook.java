package com.cdmzl.library.domain;

import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.cdmzl.common.actable.annotation.Column;
import com.cdmzl.common.actable.annotation.ColumnComment;
import com.cdmzl.common.actable.annotation.Table;
import com.cdmzl.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 书籍
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table
public class LryBook extends BaseEntity {

    @TableId
    private Long bookId;

    /**
     * 书籍的标题。
     */
    private String title;

    /**
     * 书籍的作者。
     */
    private String author;

    /**
     * 书籍的国际标准书号（ISBN）。
     */
    private String isbn;

    /**
     * 书籍的出版日期。
     */
    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN, timezone = "GMT+8")
    @DateTimeFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    private Date publicationDate;

    /**
     * 书籍的详细描述。
     */
    private String description;

    /**
     * 商品编码（可能是条形码或库存编号）
     */
    @ColumnComment("图书编号")
    private String bookCode;
    /**
     * 出版社的名称
     */
    private String publisher;

    /**
     * 书籍的正文语言
     */
    private String language;

    /**
     * 库存
     */
    private Integer inventory;

    /**
     * 可借数量
     */
    private Integer borrowNum;

    /**
     * 书本价值
     */
    private BigDecimal price;

    /**
     * 删除标记
     */
    @TableLogic
    @Column(defaultValue = "1")
    private String tag;

}
