package com.cdmzl.library.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.cdmzl.common.actable.annotation.Column;
import com.cdmzl.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 门禁记录
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LryAccessRecord extends BaseEntity {

    @TableId
    private Long accessRecordId;

    /**
     * 人员id
     */
    private Long userId;

    private String nickName;

    /**
     * 进0 出1
     */
    private String status;

    /**
     * 删除标记
     */
    @TableLogic
    @Column(defaultValue = "1")
    private String tag;

}
