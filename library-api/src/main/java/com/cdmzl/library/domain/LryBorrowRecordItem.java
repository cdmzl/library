package com.cdmzl.library.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.cdmzl.common.actable.annotation.Column;
import com.cdmzl.common.actable.annotation.Table;
import lombok.Data;

@Data
@Table(comment = "订单记录")
public class LryBorrowRecordItem {

    @TableId
    private Long borrowRecordItemId;

    private Long borrowRecordId;

    private Long bookId;

    private String bookTitle;
    /**
     * 数量
     */
    private Integer quantity;

    /**
     * 删除标记
     */
    @TableLogic
    @Column(defaultValue = "1")
    private String tag;
}
