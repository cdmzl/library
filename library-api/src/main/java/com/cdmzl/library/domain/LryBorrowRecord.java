package com.cdmzl.library.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.cdmzl.common.actable.annotation.Column;
import com.cdmzl.common.actable.annotation.Table;
import com.cdmzl.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 借阅记录
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table
public class LryBorrowRecord extends BaseEntity {

    @TableId
    private Long borrowRecordId;

    private Long userId;

    private String nickName;

    /**
     * 开始借书
     */
    private Date startTime;

    /**
     * 结束阅读时间
     */
    private Date endTime;

    /**
     * 借阅状态
     */
    private String status;

    /**
     * 删除标记
     */
    @TableLogic
    @Column(defaultValue = "1")
    private String tag;

    /**
     * 借阅书籍信息list
     */
    @TableField(exist = false)
    private List<LryBorrowRecordItem> bookList;

}
