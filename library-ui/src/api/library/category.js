import request from '@/utils/request'

// 查询床位列表
export function listCategory(query) {
  return request({
    url: '/library/category/list',
    method: 'get',
    params: query
  })
}

// 查询床位列表
export function selectCategoryTreeList(query) {
  return request({
    url: '/library/category/selectCategoryTreeList',
    method: 'get',
    params: query
  })
}

// 查询床位详细
export function getCategory(id) {
  return request({
    url: '/library/category/getById/' + id,
    method: 'get'
  })
}

// 新增床位
export function addCategory(data) {
  return request({
    url: '/library/category',
    method: 'post',
    data: data
  })
}

// 修改床位
export function updateCategory(data) {
  return request({
    url: '/library/category',
    method: 'put',
    data: data
  })
}

// 删除床位
export function delCategory(id) {
  return request({
    url: '/library/category/removeById/' + id,
    method: 'delete'
  })
}
