import request from '@/utils/request'

// 查询床位列表
export function pageAccess(query) {
  return request({
    url: '/library/access/page',
    method: 'get',
    params: query
  })
}

// 查询床位详细
export function getAccess(id) {
  return request({
    url: '/library/access/getById/' + id,
    method: 'get'
  })
}

// 新增床位
export function addAccess(data) {
  return request({
    url: '/library/access',
    method: 'post',
    data: data
  })
}

// 修改床位
export function updateAccess(data) {
  return request({
    url: '/library/access',
    method: 'put',
    data: data
  })
}

// 删除床位
export function delAccess(id) {
  return request({
    url: '/library/access/removeById/' + id,
    method: 'delete'
  })
}
