import request from '@/utils/request'

// 查询借阅书籍列表
export function pageBorrowItem(query) {
  return request({
    url: '/library/borrowItem/page',
    method: 'get',
    params: query
  })
}

// 查询借阅书籍详细
export function getBorrowItem(id) {
  return request({
    url: '/library/borrowItem/getById/' + id,
    method: 'get'
  })
}

// 新增借阅书籍
export function addBorrowItem(data) {
  return request({
    url: '/library/borrowItem',
    method: 'post',
    data: data
  })
}

// 修改借阅书籍
export function updateBorrowItem(data) {
  return request({
    url: '/library/borrowItem',
    method: 'put',
    data: data
  })
}

// 删除借阅书籍
export function delBorrowItem(id) {
  return request({
    url: '/library/borrowItem/removeById/' + id,
    method: 'delete'
  })
}
