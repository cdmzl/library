import request from '@/utils/request'

// 查询座位列表
export function pageSeatRecord(query) {
  return request({
    url: '/library/seatRecord/page',
    method: 'get',
    params: query
  })
}

// 查询座位详情
export function getSeatRecord(id) {
  return request({
    url: '/library/seatRecord/getById/' + id,
    method: 'get'
  })
}

// 新增座位
export function addSeatRecord(data) {
  return request({
    url: '/library/seatRecord',
    method: 'post',
    data: data
  })
}

// 修改座位记录
export function updateSeatRecord(data) {
  return request({
    url: '/library/seatRecord',
    method: 'put',
    data: data
  })
}

// 修改座位记录
export function delSeatRecord(id) {
  return request({
    url: '/library/seatRecord/removeById/' + id,
    method: 'delete'
  })
}
