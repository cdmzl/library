import request from '@/utils/request'

// 查询床位列表
export function pageBook(query) {
  return request({
    url: '/library/book/page',
    method: 'get',
    params: query
  })
}

// 查询床位详细
export function getBook(id) {
  return request({
    url: '/library/book/getById/' + id,
    method: 'get'
  })
}

// 新增床位
export function addBook(data) {
  return request({
    url: '/library/book',
    method: 'post',
    data: data
  })
}

// 修改床位
export function updateBook(data) {
  return request({
    url: '/library/book',
    method: 'put',
    data: data
  })
}

// 删除床位
export function delBook(id) {
  return request({
    url: '/library/book/removeById/' + id,
    method: 'delete'
  })
}
