import request from '@/utils/request'

// 查询床位列表
export function pageSeat(query) {
  return request({
    url: '/library/seat/page',
    method: 'get',
    params: query
  })
}

// 查询床位详细
export function getSeat(id) {
  return request({
    url: '/library/seat/getById/' + id,
    method: 'get'
  })
}

// 新增床位
export function addSeat(data) {
  return request({
    url: '/library/seat',
    method: 'post',
    data: data
  })
}

// 修改床位
export function updateSeat(data) {
  return request({
    url: '/library/seat',
    method: 'put',
    data: data
  })
}

// 删除床位
export function delSeat(id) {
  return request({
    url: '/library/seat/removeById/' + id,
    method: 'delete'
  })
}
