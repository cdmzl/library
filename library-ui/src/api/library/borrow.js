import request from '@/utils/request'

// 查询床位列表
export function pageBorrow(query) {
  return request({
    url: '/library/borrow/page',
    method: 'get',
    params: query
  })
}

// 查询床位详细
export function getBorrow(id) {
  return request({
    url: '/library/borrow/getById/' + id,
    method: 'get'
  })
}

// 新增床位
export function addBorrow(data) {
  return request({
    url: '/library/borrow',
    method: 'post',
    data: data
  })
}

// 修改床位
export function updateBorrow(data) {
  return request({
    url: '/library/borrow',
    method: 'put',
    data: data
  })
}

// 删除床位
export function delBorrow(id) {
  return request({
    url: '/library/borrow/removeById/' + id,
    method: 'delete'
  })
}
