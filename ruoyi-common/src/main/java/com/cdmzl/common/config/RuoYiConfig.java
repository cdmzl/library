package com.cdmzl.common.config;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 *
 * @author ruoyi
 */

@Data
@Component
@ConfigurationProperties(prefix = "cdmzl")
public class RuoYiConfig {

    /**
     * 项目名称
     */
    private String name;
    /**
     * 系统名称
     */
    private String title;

    /**
     * 版本
     */
    private String version;

    /**
     * 版权年份
     */
    private String copyrightYear;

    /**
     * 实例演示开关
     */
    private boolean demoEnabled;

    /**
     * 缓存懒加载
     */
    private boolean cacheLazy;

    /**
     * 文件缓存路径
     */
    private String filePath;

    /**
     * 存储地址
     */
    private String endpoint;


    /**
     * 获取地址开关
     */
    @Getter
    private static boolean addressEnabled;

    public void setAddressEnabled(boolean addressEnabled) {
        RuoYiConfig.addressEnabled = addressEnabled;
    }

}
